variable "yaml_file" {
    type = string
    default = null
}

locals {
    yaml_vars = yamldecode(file("${path.module}/../${var.yaml_file}"))
}