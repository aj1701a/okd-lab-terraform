provider "vsphere" {
  user           = local.yaml_vars.vsphere.username
  password       = local.yaml_vars.vsphere.password
  vsphere_server = local.yaml_vars.vsphere.server

  # If you have a self-signed cert
  allow_unverified_ssl = true
}

# provider "infoblox" {
#   INFOBLOX_USERNAME = "infoblox"
#   INFOBLOX_SERVER = "10.0.0.1"
#   INFOBLOX_PASSWORD = "infoblox"
# }
