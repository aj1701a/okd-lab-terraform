#Main.tf
#Where the magic happens!
#variables.tf and providers.tf are automagically loaded in

module "vcenter_folder" {
  source = "./modules/vcenter_folder/"
  vsphere = local.yaml_vars.vsphere
  cluster = local.yaml_vars.cluster
}

module "bootstrap" {
  source = "./modules/node_bare_metal/"
  vsphere = local.yaml_vars.vsphere
  cluster = local.yaml_vars.cluster
  vm_name = "bootstrap"
  #needed for correct order of operations so that the folder in vCenter is considered a dependency and created first
  folder = module.vcenter_folder.output
  ignition_url = format("%s/bootstrap.ign",local.yaml_vars.cluster.ignition_base_url)
}

module "control_plane" {
  source = "./modules/node_bare_metal/"
  vsphere = local.yaml_vars.vsphere
  cluster = local.yaml_vars.cluster
  vm_name = "control-plane"
  #needed for correct order of operations so that the folder in vCenter is considered a dependency and created first
  folder = module.vcenter_folder.output
  ignition_url = format("%s/master.ign",local.yaml_vars.cluster.ignition_base_url)
}

module "worker" {
  source = "./modules/node_bare_metal/"
  vsphere = local.yaml_vars.vsphere
  cluster = local.yaml_vars.cluster
  vm_name = "worker"
  #needed for correct order of operations so that the folder in vCenter is considered a dependency and created first
  folder = module.vcenter_folder.output
  ignition_url = format("%s/worker.ign",local.yaml_vars.cluster.ignition_base_url)
}