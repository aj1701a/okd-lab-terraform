variable "cluster" {}

variable "vsphere" {}

data "vsphere_datacenter" "dc" {
  name = var.vsphere.datacenter
}

resource "vsphere_folder" "folder" {
  path          = var.cluster.name
  type          = "vm"
  datacenter_id = data.vsphere_datacenter.dc.id
}

output "output" {
  value = vsphere_folder.folder.path
}