data "vsphere_datacenter" "dc" {
  name = var.vsphere.datacenter
}

data "vsphere_datastore" "virtual_disk_datastore" {
  name = var.vsphere.virtual_disk_datastore
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_datastore" "iso_datastore" {
  name = var.vsphere.baremetal.iso_datastore
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_resource_pool" "pool" {
  name          = format("%s/Resources", var.vsphere.cluster_name)
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_network" "node_network" {
  name          = var.vsphere.node_network_name
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_virtual_machine" "template" {
  name          = var.vsphere.rhcos_template_name
  datacenter_id = data.vsphere_datacenter.dc.id
}

resource "vsphere_virtual_machine" "vm" {
  #Count is a built in var! Its like magic!
  count            = var.cluster["${var.vm_name}-count"]
  name             = format("%s-%s", var.vm_name, count.index)
  resource_pool_id = data.vsphere_resource_pool.pool.id
  datastore_id     = data.vsphere_datastore.virtual_disk_datastore.id
  folder           = var.folder
  num_cpus         = var.cluster.vm_resources[var.vm_name].cpu
  memory           = var.cluster.vm_resources[var.vm_name].memory
  guest_id         = var.vsphere.guest_type

  network_interface {
    network_id     = data.vsphere_network.node_network.id
    adapter_type   = data.vsphere_virtual_machine.template.network_interface_types[0]
  }

  disk {
    label = "disk0"
    size  = var.cluster.vm_resources[var.vm_name].disk
    thin_provisioned = true
  }

  cdrom {
    datastore_id = data.vsphere_datastore.iso_datastore.id
    path         = var.vsphere.baremetal.iso_path
  }

  //Disable timeouts
  wait_for_guest_net_timeout = 0
  wait_for_guest_net_routable = false
  wait_for_guest_ip_timeout = 0
}

resource "null_resource" "ansible" {
  depends_on = [vsphere_virtual_machine.vm]
  count            = var.cluster["${var.vm_name}-count"]

  provisioner "local-exec" {
    command = "ansible-playbook ../ansible/rhcos-set-kernel-parameters.yaml -e \"vars_file=../homelab.vars.yaml ocp_vm_name=${format("%s-%s", var.vm_name, count.index) } node_ip=${var.cluster.hard_coded_ips[format("%s-%s", var.vm_name, count.index)]}\""
    interpreter = ["bash", "-c"]
  }
}