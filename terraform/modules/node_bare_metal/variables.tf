variable "cluster" {}

variable "vsphere" {}

variable "folder" {}

variable "vm_name" {}

variable "ignition_url" {
  type    = string
  default = ""
}

variable "ip_address" {
  type    = string
  default = ""
}