provider "ignition" {
  #version = "1.1.0"
}

locals {
  //ip_address = var.cluster.hard_coded_ips[format("%s-%s", var.vm_name, count.index)]
  ignition_encoded = "data:text/plain;charset=utf-8;base64,${base64encode(var.ignition)}"
}

data "ignition_file" "hostname" {
  count      = var.cluster["${var.vm_name}-count"]
  filesystem = "root"
  path       = "/etc/hostname"
  mode       = "420"

  content {
    content = format("%s-%s.%s.%s", var.vm_name, count.index, var.cluster.name, var.cluster.domain)
  }
}

data "ignition_networkd_unit" "networkd_static_ip" {
    count = var.cluster["${var.vm_name}-count"]
    name = "00-ens192.network"
    content = <<EOF
[Match]
Name=ens192

[Network]
Address=${var.cluster.hard_coded_ips[format("%s-%s", var.vm_name, count.index)]}/${element(split("/", var.cluster.networks.node_cidr), 1)}
Gateway=${var.cluster.gateway}
DNS=${var.cluster.nameserver}
Domains=${var.cluster.name}.${var.cluster.domain}
EOF
}


data "ignition_file" "static_ip_conf" {
  count      = var.cluster["${var.vm_name}-count"]
  filesystem = "root"
  path       = "/etc/sysconfig/network-scripts/ifcfg-ens192"
  mode       = "420"

  content {
    content = <<EOF
TYPE=Ethernet
BOOTPROTO=none
NAME=ens192
DEVICE=ens192
ONBOOT=yes
IPV6INIT=no
IPV6_AUTOCONF=no
IPADDR=${var.cluster.hard_coded_ips[format("%s-%s", var.vm_name, count.index)]}
PREFIX=${element(split("/", var.cluster.networks.node_cidr), 1)}
GATEWAY=${var.cluster.gateway}
DOMAIN=${var.cluster.name}.${var.cluster.domain}
DNS1=${var.cluster.nameserver}
EOF
  }
}

data "ignition_systemd_unit" "restart" {
  name = "restart.service"

  content = <<EOF
[Unit]
ConditionFirstBoot=yes
[Service]
Type=idle
ExecStart=/sbin/reboot
[Install]
WantedBy=multi-user.target
EOF
}

data "ignition_config" "ign" {
  count      = var.cluster["${var.vm_name}-count"]

  append {
    source = "${var.ignition_url != "" ? var.ignition_url : local.ignition_encoded}"
  }

  # Disabled this may have been causing issues
  # systemd = [
  #   data.ignition_systemd_unit.restart.rendered,
  # ]

  files = [
    data.ignition_file.hostname[count.index].rendered,
    data.ignition_file.static_ip_conf[count.index].rendered,
  ]

  networkd = [
    data.ignition_networkd_unit.networkd_static_ip[count.index].rendered,
  ]
}