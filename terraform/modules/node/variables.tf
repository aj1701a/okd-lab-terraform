variable "cluster" {}

variable "vsphere" {}

variable "folder" {}

variable "vm_name" {}

variable "ignition" {
  type    = string
  default = ""
}

variable "ignition_url" {
  type    = string
  default = ""
}