data "vsphere_datacenter" "dc" {
  name = var.vsphere.datacenter
}

data "vsphere_datastore" "virtual_disk_datastore" {
  name = var.vsphere.virtual_disk_datastore
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_resource_pool" "pool" {
  name          = format("%s/Resources", var.vsphere.cluster_name)
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_network" "node_network" {
  name          = var.vsphere.node_network_name
  datacenter_id = data.vsphere_datacenter.dc.id
}

data "vsphere_virtual_machine" "template" {
  name          = var.vsphere.rhcos_template_name
  datacenter_id = data.vsphere_datacenter.dc.id
}

resource "vsphere_virtual_machine" "vm" {
  #Count is a built in var! Its like magic!
  count            = var.cluster["${var.vm_name}-count"]
  name             = format("%s-%s", var.vm_name, count.index)
  resource_pool_id = data.vsphere_resource_pool.pool.id
  datastore_id     = data.vsphere_datastore.virtual_disk_datastore.id
  folder           = var.folder
  num_cpus         = var.cluster.vm_resources[var.vm_name].cpu
  memory           = var.cluster.vm_resources[var.vm_name].memory
  guest_id         = var.vsphere.guest_type

  network_interface {
    network_id     = data.vsphere_network.node_network.id
    adapter_type   = data.vsphere_virtual_machine.template.network_interface_types[0]
  }

  disk {
    label = "disk0"
    size  = var.cluster.vm_resources[var.vm_name].disk
    thin_provisioned = data.vsphere_virtual_machine.template.disks.0.thin_provisioned
  }

  clone {
    template_uuid = data.vsphere_virtual_machine.template.id
  }

  vapp {
    properties = {
      "guestinfo.ignition.config.data"          = "${base64encode(data.ignition_config.ign.*.rendered[count.index])}"
      "guestinfo.ignition.config.data.encoding" = "base64"
    }
  }

  //Disable timeouts
  wait_for_guest_net_timeout = 0
  wait_for_guest_net_routable = false
  wait_for_guest_ip_timeout = 0
}