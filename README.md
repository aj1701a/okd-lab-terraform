# okd-lab-terraform

## TODO
- RHCOS Template
    - TF has a limitation with adding OVF/OVA templates only to an ESXi host not a vCenter or cluster
    - Must have an Ansible playbook or manual creation of the template
- Use Ansible to generate install-config.yml via jinja
- Use Ansible to generation ignition files with openshift-install
- DNS provision
- It may take some time to create the VMs -- ping vm's until up

## Running Terraform
terraform plan -var yaml_file="homelab.vars.yaml"
terraform apply -var yaml_file="homelab.vars.yaml"
terraform destroy -var yaml_file="homelab.vars.yaml"

## Notes
https://www.terraform.io/docs/modules/index.html  
https://www.terraform.io/docs/providers/vsphere/index.html  
https://learn.hashicorp.com/terraform/getting-started/variables.html  
https://www.terraform.io/docs/configuration/variables.html  
https://stackoverflow.com/questions/61122830/using-terraform-yamldecode-to-access-multi-level-element  
https://blog.gruntwork.io/terraform-tips-tricks-loops-if-statements-and-gotchas-f739bbae55f9  
https://emilwypych.com/2017/10/15/deploying-multiple-vsphere-vms-terraform/?cn-reloaded=1  
https://github.com/terraform-providers/terraform-provider-infoblox
https://github.com/alexgdmx/ocp4-vmware-terraform
https://veducate.co.uk/how-to-deploy-openshift-on-vmware/


## Ansible on Win10
https://www.jeffgeerling.com/blog/2017/using-ansible-through-windows-10s-subsystem-linux    


ansible-playbook ansible/test-module.yaml -e "@homelab.vars.yaml"